﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace MECLeaderboardDashes.Controllers
{
    public class AccountsController : Controller
    {
        public static void Remove(List<JObject> AllResults, int user1, int user2)
        {
            if (AllResults.Exists(x => Convert.ToInt64(x.SelectToken("personaId")) == user1) && AllResults.Exists(x => Convert.ToInt64(x.SelectToken("personaId")) == user2))
            {
                var account1 = AllResults.Find(x => Convert.ToInt64(x.SelectToken("personaId")) == user1);
                var account2 = AllResults.Find(x => Convert.ToInt64(x.SelectToken("personaId")) == user2);

                if (Convert.ToInt32(account1.SelectToken("score")) > Convert.ToInt32(account2.SelectToken("score")))
                {
                    AllResults.RemoveAt(AllResults.FindIndex(w => Convert.ToInt64(w.SelectToken("personaId")) == user1));
                }
                else if ((Convert.ToInt32(account1.SelectToken("score")) < Convert.ToInt32(account2.SelectToken("score"))))
                {
                    AllResults.RemoveAt(AllResults.FindIndex(w => Convert.ToInt64(w.SelectToken("personaId")) == user2));
                }
            }
        }
    }
}