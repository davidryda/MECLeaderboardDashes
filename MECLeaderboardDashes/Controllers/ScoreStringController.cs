﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MECLeaderboardDashes.Controllers
{
    public class ScoreStringController : Controller
    {
        // GET: ScoreString
        public static string ScoreFormatter(int score)
        {
            var first = Math.Floor((decimal)score / 6000);
            string secEnd = Math.Abs((decimal)score % 6000).ToString("0000"); //number % 100 = last two digits
            string finalScore = (first >= 1) ? first + ":" + secEnd.Insert(2, ".") : secEnd.Insert(2, ".");

            return finalScore;
        }
    }
}