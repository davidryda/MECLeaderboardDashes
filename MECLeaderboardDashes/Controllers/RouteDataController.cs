﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MECLeaderboardDashes.Controllers
{
    public class RouteDataController : Controller
    {
        // GET: RouteData
        public static string LeaderboardList(string dashID, string platform)
        {
            HttpWebRequest WR = WebRequest.CreateHttp("https://mec-gw.ops.dice.se/jsonrpc/prod_default/prod_default/" + platform + "/api");
            WR.ContentType = "application/json";
            WR.Method = "POST";

            using (var streamWriter = new StreamWriter(WR.GetRequestStream()))
            {
                var json = "{\"jsonrpc\":\"2.0\",\"method\":\"Pamplona.getRunnersRouteLeaderboard\",\"params\":{ \"challengeId\":\"" + dashID + "\"}}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            WR.Accept = "application/json";

            WR.UserAgent = ".Net Test Framework";

            HttpWebResponse Response = (HttpWebResponse)WR.GetResponse();

            StreamReader Reader = new StreamReader(Response.GetResponseStream());

            string data = Reader.ReadToEnd();

            return data;
        }
    }
}