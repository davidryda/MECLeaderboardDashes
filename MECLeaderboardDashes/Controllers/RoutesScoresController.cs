﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MECLeaderboardDashes.Controllers
{
    public class RoutesScoresController : Controller
    {
        // GET: RoutesScores
        public static string RoutesScores(string playerID, string platforms)
        {
            HttpWebRequest WR = WebRequest.CreateHttp("https://mec-gw.ops.dice.se/jsonrpc/prod_default/prod_default/" + platforms + "/api");
            WR.ContentType = "application/json";
            WR.Method = "POST";

            using (var streamWriter = new StreamWriter(WR.GetRequestStream()))
            {
                var json = "{\"jsonrpc\":\"2.0\",\"method\":\"Pamplona.getRunnersRouteData\",\"params\":{\"challengeIds\":[\"ch_rrt_tv2_time\",\"ch_rrt_tv04_time\",\"ch_rrt_dt4_time\",\"ch_rrt_tv3_time\",\"ch_rrt_anc1_time\",\"ch_rrt_dt1_time\",\"ch_rrt_rz3_time\",\"ch_rrt_tv05_time\",\"ch_rrt_dt2_time\",\"ch_rrt_rz4_time\",\"ch_rrt_dt3_time\",\"ch_rrt_anc4_time\",\"ch_rrt_anc5_time\",\"ch_rrt_bm1_time\",\"ch_rrt_anc6_time\",\"ch_rrt_tv1_time\",\"ch_rrt_dt6_time\",\"ch_rrt_anc2_time\",\"ch_rrt_rz2_time\",\"ch_rrt_anc3_time\",\"ch_rrt_dt5_time\",\"ch_rrt_rz1_time\"],\"dataTypes\":[\"USER_STATS\"],\"personaId\":\"" + playerID + "\"}}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            WR.Accept = "application/json";

            WR.UserAgent = ".Net Test Framework";

            HttpWebResponse Response = (HttpWebResponse)WR.GetResponse();

            StreamReader Reader = new StreamReader(Response.GetResponseStream());

            string data = Reader.ReadToEnd();

            return data;
        }
    }
}