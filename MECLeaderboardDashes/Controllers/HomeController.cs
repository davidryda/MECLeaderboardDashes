﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MECLeaderboardDashes.Controllers
{
    public class HomeController : Controller
    {
        public static string levels = "Caleb's Run,Don't Fall Down,Backstreet Bluff,Noah's Run,The Allcom Shuffle,High Roller Avenue,Under Construction,The Scenic Route,Concrete Canyon,Rezoning Dash,Nomads Run,Quite a View,Heading Home,Birdman's Route,Donkey in an Oven,Too Close to the Sun,Feature Creep,Take Me to the Gridnode,Old Tunnels,Consumer Mayhem,A Handy Shortcut,Out in the Open";

        public List<string> levelList = new List<string>(levels.Split(','));

        public static string levelCodes = "ch_rrt_tv2_time,ch_rrt_tv04_time,ch_rrt_dt4_time,ch_rrt_tv3_time,ch_rrt_anc1_time,ch_rrt_dt1_time,ch_rrt_rz3_time,ch_rrt_tv05_time,ch_rrt_dt2_time,ch_rrt_rz4_time,ch_rrt_dt3_time,ch_rrt_anc4_time,ch_rrt_anc5_time,ch_rrt_bm1_time,ch_rrt_anc6_time,ch_rrt_tv1_time,ch_rrt_dt6_time,ch_rrt_anc2_time,ch_rrt_rz2_time,ch_rrt_anc3_time,ch_rrt_dt5_time,ch_rrt_rz1_time";

        public List<string> levelCodesList = new List<string>(levelCodes.Split(','));

        public string[] platforms = new string[] { "pc", "ps4", "xboxone" };

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult Versus()
        {
            return View();
        }

        public ActionResult VersusBattle(string playerID1, string playerID2)
        {
            var Player1Scores = new List<string>();
            var Player2Scores = new List<string>();

            for (int z = 0; z < platforms.Length; z++) //gets route ids and scores
            {
                var data = RoutesScoresController.RoutesScores(playerID1, platforms[z]);

                JObject WebData = JObject.Parse(data);

                foreach (var result in WebData["result"])
                {
                    if (result != null)
                    {
                        Player1Scores.Add((string)result.SelectToken("userRank.score"));
                    }
                    else
                    {
                        continue;
                    }
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                var data2 = RoutesScoresController.RoutesScores(playerID2, platforms[z]);

                JObject WebData2 = JObject.Parse(data2);

                foreach (var result in WebData2["result"])
                {
                    if (result != null)
                    {
                        Player2Scores.Add((string)result.SelectToken("userRank.score"));
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            string APIName1 = null;
            string APIName2 = null;

            for (int z = 0; z < platforms.Length; z++)
            {
                APIName1 = GetNameController.GetName(playerID1, platforms[z]);
                APIName2 = GetNameController.GetName(playerID2, platforms[z]);
            }

            ViewBag.APIName1 = APIName1;
            ViewBag.APIName2 = APIName2;

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            ViewBag.LevelName = levelList;

            var Player1ScoresInt = new List<int>();
            var Player2ScoresInt = new List<int>();

            foreach (var score in Player1Scores)
            {
                if (Convert.ToInt32(score) != 0)
                {
                    Player1ScoresInt.Add(Convert.ToInt32(score));
                }
            }

            foreach (var score in Player2Scores)
            {
                if (Convert.ToInt32(score) != 0)
                {
                    Player2ScoresInt.Add(Convert.ToInt32(score));
                }
            }

            var ScoreDifference = new List<string>();

            for (int i = 0; i < 22; i++)
            {
                if (Player1ScoresInt[i] < Player2ScoresInt[i])
                {
                    ScoreDifference.Add(ScoreStringController.ScoreFormatter(Player2ScoresInt[i] - Player1ScoresInt[i]));
                }
                else if (Player2ScoresInt[i] < Player1ScoresInt[i])
                {
                    ScoreDifference.Add(ScoreStringController.ScoreFormatter(Player1ScoresInt[i] - Player2ScoresInt[i]));
                }
                else
                {
                    ScoreDifference.Add(APIName1 + " and " + APIName2 + " are equal!");
                }
            }

            ViewBag.ScoreDifference = ScoreDifference;

            var Player1ScoresTotal = Player1ScoresInt.Sum();
            var Player2ScoresTotal = Player2ScoresInt.Sum();

            int firstPlace = Math.Min(Player1ScoresInt.Sum(), Player2ScoresInt.Sum());
            int lastPlace = Math.Max(Player1ScoresInt.Sum(), Player2ScoresInt.Sum());
            int finalNumber = lastPlace - firstPlace;
            string finalResult = ScoreStringController.ScoreFormatter(finalNumber);

            if (Player1ScoresInt.Sum() < Player2ScoresInt.Sum())
            {
                ViewBag.Win = ViewBag.APIName1 + " is faster by " + finalResult;
            }
            else if (Player1ScoresInt.Sum() > Player2ScoresInt.Sum())
            {
                ViewBag.Win = ViewBag.APIName2 + " is faster by " + finalResult;
            }
            else
            {
                ViewBag.Win = ViewBag.APIName1 + " and " + ViewBag.APIName2 + " are tied!";
            }

            ViewBag.Player1ScoresTotal = ScoreStringController.ScoreFormatter(Player1ScoresTotal);
            ViewBag.Player2ScoresTotal = ScoreStringController.ScoreFormatter(Player2ScoresTotal);

            List<string> Player1ScoresString = new List<string>();
            List<string> Player2ScoresString = new List<string>();

            foreach (var score in Player1ScoresInt)
            {
                string finalScore = ScoreStringController.ScoreFormatter(score);
                Player1ScoresString.Add(finalScore);
            }

            foreach (var score in Player2ScoresInt)
            {
                string finalScore = ScoreStringController.ScoreFormatter(score);
                Player2ScoresString.Add(finalScore);
            }

            ViewBag.Player1Scores = Player1ScoresString;
            ViewBag.Player2Scores = Player2ScoresString;

            ViewBag.Player1ScoresInt = Player1ScoresInt;
            ViewBag.Player2ScoresInt = Player2ScoresInt;

            return View();
        }

        public ActionResult SearchResults(string playerID, string platform)
        {
            List<JObject> AllResults = new List<JObject>();

            List<int> IndividualRuns = new List<int>();

            Dictionary<string, string> levelDictionary = new Dictionary<string, string>();

            levelDictionary.Add("ch_rrt_tv2_time", "Caleb's Run");
            levelDictionary.Add("ch_rrt_tv04_time", "Don't Fall Down");
            levelDictionary.Add("ch_rrt_dt4_time", "Backstreet Bluff");
            levelDictionary.Add("ch_rrt_tv3_time", "Noah's Run");
            levelDictionary.Add("ch_rrt_anc1_time", "The Allcom Shuffle");
            levelDictionary.Add("ch_rrt_dt1_time", "High Roller Avenue");
            levelDictionary.Add("ch_rrt_rz3_time", "Under Construction");
            levelDictionary.Add("ch_rrt_tv05_time", "The Scenic Route");
            levelDictionary.Add("ch_rrt_dt2_time", "Concrete Canyon");
            levelDictionary.Add("ch_rrt_rz4_time", "Rezoning Dash");
            levelDictionary.Add("ch_rrt_dt3_time", "Nomads Run");
            levelDictionary.Add("ch_rrt_anc4_time", "Quite a View");
            levelDictionary.Add("ch_rrt_anc5_time", "Heading Home");
            levelDictionary.Add("ch_rrt_bm1_time", "Birdman's Route");
            levelDictionary.Add("ch_rrt_anc6_time", "Donkey in an Oven");
            levelDictionary.Add("ch_rrt_tv1_time", "Too Close to the Sun");
            levelDictionary.Add("ch_rrt_dt6_time", "Feature Creep");
            levelDictionary.Add("ch_rrt_anc2_time", "Take Me to the Gridnode");
            levelDictionary.Add("ch_rrt_rz2_time", "Old Tunnels");
            levelDictionary.Add("ch_rrt_anc3_time", "Consumer Mayhem");
            levelDictionary.Add("ch_rrt_dt5_time", "A Handy Shortcut");
            levelDictionary.Add("ch_rrt_rz1_time", "Out in the Open");

            for (int z = 0; z < platforms.Length; z++) //gets route ids and scores
            {
                string data = RoutesScoresController.RoutesScores(playerID, platforms[z]);

                JObject WebData = JObject.Parse(data);

                foreach (var result in WebData["result"])
                {
                    if (result["userRank"].SelectToken("score") != null)
                    {
                        AllResults.Add((JObject)result);
                    }
                }
            }

            foreach (var a in AllResults)
            {
                a["id"].Replace(levelDictionary[(a["id"].ToString())]);
                IndividualRuns.Add((int)a["userRank"].SelectToken("score"));
                a["userRank"].SelectToken("score").Replace(ScoreStringController.ScoreFormatter((int)a["userRank"].SelectToken("score")));
                a["userStats"].SelectToken("finishedAt").Replace(new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToDouble(a["userStats"].SelectToken("finishedAt"))));
            }

            var total = Convert.ToDecimal(IndividualRuns.Sum());
            var totalMinutes = Math.Floor(total / 6000);
            string totalSecEnd = Math.Abs(total % 6000).ToString("0000");
            ViewBag.totalScore = totalMinutes + ":" + totalSecEnd.Insert(2, ".");

            for (int z = 0; z < platforms.Length; z++) //gets name
            {
                string APIName = GetNameController.GetName(playerID, platforms[z]);

                ViewBag.APIName = APIName;
            }

            ViewBag.Platform = platform;

            ViewBag.AllResults = AllResults;

            ViewBag.LevelList = levelList;

            return View();
        }

        public ActionResult ViewDashes()
        {
            ViewBag.Leaderboard = levelCodesList;

            ViewBag.Name = levelList;

            return View();
        }

        public ActionResult ViewConsoleOnlyDashesList()
        {
            ViewBag.Leaderboard = levelCodesList;

            ViewBag.Name = levelList;

            return View("ConsoleOnlyDashesList");
        }

        public ActionResult ViewBirdman(string dashID, string consoleOnly)
        {
            string ban = "173113591,1779625318,190234115,293318433,1023303792,1120580501,1698113918,297810190,234398919,740326190,391414636,774371554,936727109,1744895314,1058799702,914014728,280049454,320947648,324211297,912419706,997726181,1023303792,1141884688,275159673,866468390,1487180098,186053875,980924768,226470327,1014287677,371870382,294957347,285272823,431042609,1002600677,1761018786,1743717653,1856026993,255203621,1551614516,394079870,340142948,942923237,1746195635,527735064,1221368608,315761875,369033315,851610928,175573719,1773731047,332267702,1639629365,354418410,363004584,420258019,1007149794,546600601,926090737,1108107459,534659528,257375731,270692819,286967298,326073660,859634210,269592602,821666745,329249954,354509877,1726323902,994589619,363591482,424579775,1781465159,1919088615,1968834133,392004241,1872541288"; //1968834133(derwangler ps4)

            List<string> banList = new List<string>(ban.Split(','));

            ViewBag.BanList = banList;

            Dictionary<string, string> levelDictionary = new Dictionary<string, string>();

            levelDictionary.Add("ch_rrt_tv2_time", "Caleb's Run");
            levelDictionary.Add("ch_rrt_tv04_time", "Don't Fall Down");
            levelDictionary.Add("ch_rrt_dt4_time", "Backstreet Bluff");
            levelDictionary.Add("ch_rrt_tv3_time", "Noah's Run");
            levelDictionary.Add("ch_rrt_anc1_time", "The Allcom Shuffle");
            levelDictionary.Add("ch_rrt_dt1_time", "High Roller Avenue");
            levelDictionary.Add("ch_rrt_rz3_time", "Under Construction");
            levelDictionary.Add("ch_rrt_tv05_time", "The Scenic Route");
            levelDictionary.Add("ch_rrt_dt2_time", "Concrete Canyon");
            levelDictionary.Add("ch_rrt_rz4_time", "Rezoning Dash");
            levelDictionary.Add("ch_rrt_dt3_time", "Nomads Run");
            levelDictionary.Add("ch_rrt_anc4_time", "Quite a View");
            levelDictionary.Add("ch_rrt_anc5_time", "Heading Home");
            levelDictionary.Add("ch_rrt_bm1_time", "Birdman's Route");
            levelDictionary.Add("ch_rrt_anc6_time", "Donkey in an Oven");
            levelDictionary.Add("ch_rrt_tv1_time", "Too Close to the Sun");
            levelDictionary.Add("ch_rrt_dt6_time", "Feature Creep");
            levelDictionary.Add("ch_rrt_anc2_time", "Take Me to the Gridnode");
            levelDictionary.Add("ch_rrt_rz2_time", "Old Tunnels");
            levelDictionary.Add("ch_rrt_anc3_time", "Consumer Mayhem");
            levelDictionary.Add("ch_rrt_dt5_time", "A Handy Shortcut");
            levelDictionary.Add("ch_rrt_rz1_time", "Out in the Open");

            string dash = levelDictionary[dashID];

            ViewBag.Dash = dash;

            ViewBag.DashID = dashID;

            Dictionary<string, string> levelID = new Dictionary<string, string>();

            levelID.Add("ch_rrt_tv2_time", "5wkr1v29");
            levelID.Add("ch_rrt_tv04_time", "69z4n0lw");
            levelID.Add("ch_rrt_dt4_time", "xd43rm29");
            levelID.Add("ch_rrt_tv3_time", "ywe8mpyw");
            levelID.Add("ch_rrt_anc1_time", "4958ln09");
            levelID.Add("ch_rrt_dt1_time", "y9mp1v59");
            levelID.Add("ch_rrt_rz3_time", "r9g3185w");
            levelID.Add("ch_rrt_tv05_time", "owo2qxod");
            levelID.Add("ch_rrt_dt2_time", "ldyp80kd");
            levelID.Add("ch_rrt_rz4_time", "rdnp2gq9");
            levelID.Add("ch_rrt_dt3_time", "z988glg9");
            levelID.Add("ch_rrt_anc4_time", "xd073jxw");
            levelID.Add("ch_rrt_anc5_time", "5d73zrg9");
            levelID.Add("ch_rrt_bm1_time", "29vk5v3d");
            levelID.Add("ch_rrt_anc6_time", "nwl7pe09");
            levelID.Add("ch_rrt_tv1_time", "ewpq0vk9");
            levelID.Add("ch_rrt_dt6_time", "gdrq1769");
            levelID.Add("ch_rrt_anc2_time", "o9xl3009");
            levelID.Add("ch_rrt_rz2_time", "xd1581ew");
            levelID.Add("ch_rrt_anc3_time", "592on1od");
            levelID.Add("ch_rrt_dt5_time", "kwje1rn9");
            levelID.Add("ch_rrt_rz1_time", "rdq15qk9");

            string level = levelID[dashID];

            //HttpWebRequest WR = WebRequest.CreateHttp("https://www.speedrun.com/api/v1/leaderboards/m1mgl312/level/" + level + "/xk9lnj6k");
            //WR.ContentType = "application/json";
            //WR.Method = "GET";
            //WR.Accept = "application/json";
            //WR.UserAgent = ".Net Test Framework";
            //HttpWebResponse Response = (HttpWebResponse)WR.GetResponse();
            //StreamReader Reader = new StreamReader(Response.GetResponseStream());
            //string WRData = Reader.ReadToEnd();

            string WRData = null;

            using (WebClient client = new WebClient())
            {
                WRData = client.DownloadString("https://www.speedrun.com/api/v1/leaderboards/m1mgl312/level/" + level + "/xk9lnj6k");
            }


            JObject WebLink = JObject.Parse(WRData);

            ViewBag.Level = WebLink["data"];

            List<JObject> AllResults = new List<JObject>();

            List<JObject> TotalCount = new List<JObject>();

            for (int z = 0; z < platforms.Length; z++)
            {
                var data = RouteDataController.LeaderboardList(dashID, platforms[z]);

                JObject WebData = JObject.Parse(data);

                JArray myarray = (JArray)WebData["result"]["leaderboard"]["users"];

                int i = 0;

                foreach (var user in WebData["result"]["leaderboard"]["users"])
                {
                    JObject platform = (JObject)myarray[i];
                    platform.Add(new JProperty("platform", platforms[z]));
                    platform.Add(new JProperty("properScore", ScoreStringController.ScoreFormatter((int)user.SelectToken("score"))));
                    i++;
                    if ((int)user.SelectToken("score") < 10000)
                    {
                        AllResults.Add((JObject)user);
                    }
                }

                TotalCount.Add((JObject)WebData["result"]["leaderboard"]);
            }

            AccountsController.Remove(AllResults, 1699572640, 180897407); //Woody

            AccountsController.Remove(AllResults, 1793453350, 1075482525); //TopLadLuke

            AllResults = AllResults.OrderBy(x => x.SelectToken("score")).ToList();

            var ConsoleOnlyResults = AllResults.OrderBy(x => x.SelectToken("score")).ToList();

            ViewBag.Results = AllResults;

            ViewBag.AlphabeticResults = AllResults.OrderBy(x => x.SelectToken("name").ToString(), StringComparer.InvariantCultureIgnoreCase).ToList();

            ViewBag.TotalCount = TotalCount;

            for (int i = 0; i < ConsoleOnlyResults.Count; i++)
            {
                ConsoleOnlyResults.Remove(ConsoleOnlyResults.Find(x => x.SelectToken("platform").ToString() == "pc"));
            }

            ViewBag.ConsoleResults = ConsoleOnlyResults;

            if (consoleOnly == "no")
            {
                return View("ViewBirdman");
            }
            else if (consoleOnly == "yes")
            {
                return View("ConsoleOnly");
            }
            else
            {
                return null;
            }
        }
    }
}