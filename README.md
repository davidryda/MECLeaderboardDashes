# <http://catalystdashes.gearhostpreview.com>

# MECLeaderboardDashes

Due to DICE, or rather EA, dropping support for Mirror's Edge Catalyst after release, the online leaderboards for dashes have slowly filled up with cheaters. So, to solve this problem, I made this online leaderboard that removes those cheaters!

# Ok? Why does this matter?

I've wanted to do this for a while now, and figured this would help the MEC community and any new runners frustrated by the in-game leaderboards.

# How did you make this?

Everything is coded in Visual Studio using ASP.NET and C#! 

All information is requested from the official Mirror's Edge website. The website uses JSON-RPC 2.0 for call requests, and this website formats the response in a totally good looking way!
